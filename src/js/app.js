require('../../node_modules/jquery/dist/jquery.min');
require('../../node_modules/slick-carousel/slick/slick.min');
require('../../node_modules/inputmask/dist/jquery.inputmask.min');
require('green-audio-player');
require('readmore-js');

var SimpleLightbox = require('simple-lightbox');
SimpleLightbox.registerAsJqueryPlugin($);

require('./scripts');
require('./audio-player');