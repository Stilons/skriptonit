import './../sass/main.scss';
import GreenAudioPlayer from "green-audio-player";

//Подключаем SVG спрайты
function requireAll(r) {
    r.keys().forEach(r);
}

requireAll(require.context('./../assets/img/icons-svg', true, /\.svg$/));

fetch(`/assets/img/sprite.svg`).then(res => {
    return res.text();
}).then(data => {
    document.getElementById('svg-icons').innerHTML = data;
});

$(document).ready(function () {
    let switcherTrigger = $('.js-switcher-trigger'),
        bookingView = $('.js-booking-view'),
        bookingList = $('.js-booking-list');
    $(switcherTrigger).click(function () {
        if (!$(this).hasClass('_active')) {
            $(this).addClass('_active');
            $(this).siblings(switcherTrigger).removeClass('_active');
        }

        if ($(this).hasClass('_active') && $(this).hasClass('js-booking-view-btn')) {
            $(bookingView).show();
            $(bookingList).hide();
        } else {
            $(bookingView).hide();
            $(bookingList).show();
        }
    });

    //Маска телефона
    $('.phone').inputmask("+7 (999) 999-99-99");  //static mask

    //Читать подробнее
    $('.js-readmore').readmore({
        speed: 75,
        moreLink: '<button class="link">Читать далее</button>',
        lessLink: '<button class="link">Свернуть</button>'
    });

    //Слайдеры
    $('.gallery__slider').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        arrows: true,
        lazyLoad: 'ondemand',
        prevArrow: '<button type="button" class="slick-arrow _prev"><svg class="icon-arrow"><use xlink:href="#arrow" /></svg></button>',
        nextArrow: '<button type="button" class="slick-arrow _next"><svg class="icon-arrow"><use xlink:href="#arrow" /></svg></button>',
        responsive: [
            {
                breakpoint: 1310,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 1019,
                settings: {
                    arrows: false,
                    slidesToShow: 3.3,
                    slidesToScroll: 3,
                }
            },
        ]
    });

    $('.news__slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: true,
        prevArrow: '<button type="button" class="slick-arrow _prev"><svg class="icon-arrow"><use xlink:href="#arrow" /></svg></button>',
        nextArrow: '<button type="button" class="slick-arrow _next"><svg class="icon-arrow"><use xlink:href="#arrow" /></svg></button>',
        responsive: [
            {
                breakpoint: 1910,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 1019,
                settings: "unslick"
            },
        ]
    });

    //lightbox
    let lightboxGallery = $('.gallery__slider-item .gallery__slider-link').simpleLightbox();

    //Плюс и минус
    let plusBtn = $('.js-plus-btn'),
        bookingChangeBtn = $('.js-booking-change'),
        minusBtn = $('.js-minus-btn');

    $(bookingChangeBtn).click(function () {
        $(this).addClass('hidden');
        $(this).siblings('.js-inner-count').removeClass('hidden');
    });

    $(plusBtn).click(function () {
        $(this).prev().text(+$(this).prev().text() + 1);
        if ($(this).prev().text() > 0) {
            $(minusBtn).addClass('_special');
        }
        else {
            $(minusBtn).removeClass('_special');
        }
    });

    $(minusBtn).click(function () {
        if ($(this).next().text() > 0) {
            $(this).next().text(+$(this).next().text() - 1);
            $(minusBtn).addClass('_special');
        }
        else {
            $(minusBtn).removeClass('_special');
        }
    });

    //Кнопка наверх
    let button = $('#button-up');
    $(window).scroll (function () {
        if ($(this).scrollTop () > 500) {
            button.fadeIn().css('display', 'flex');
        } else {
            button.fadeOut();
        }
    });
    button.on('click', function(){
        $('body, html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    //Добавляем эффекты для плеера
    let musicPlayerBtn = $('.play-pause-btn');
    $(musicPlayerBtn).click(function () {
        $(this).toggleClass('_active');
    });

    //Мобильная версия бронирования
    if ($(window).width() <= 1020) {
        $('.js-booking-btn').click(function (e) {
            e.preventDefault();
            $('.js-booking').removeClass('hidden-mobile');
        });
        $('.js-close-booking').click(function (e) {
            e.preventDefault();
            $('.js-booking').addClass('hidden-mobile');
        })
    }

    //Переключение табов на оформлении заказа
    let switcherTriggerOrder = $('.js-switcher-trigger-order'),
        switcherTriggerTickets = $('.js-switcher-trigger-tickets'),
        switcherTriggerDelivery = $('.js-switcher-trigger-delivery');
    $(switcherTriggerOrder).click(function () {
        event.preventDefault();
        if (!$(this).hasClass('_active')) {
            $(this).addClass('_active');
            $(this).siblings(switcherTriggerOrder).removeClass('_active');
        }

        if ($(this).hasClass('_active') && $(this).hasClass('js-switcher-trigger-order-btn')) {
            $(switcherTriggerTickets).show();
            $(switcherTriggerDelivery).hide();
        } else {
            $(switcherTriggerTickets).hide();
            $(switcherTriggerDelivery).show();
        }
    });

    //Открываем оформление заказа
    let modalOrderBtn = $('.js-modal-order-btn'),
        modalOrder = $('.js-modal-order'),
        modal = $('.js-modal'),
        overlay = $('.js-overlay'),
        modalClose = $('.js-close');
    $(modalOrderBtn).click(function (e) {
        e.preventDefault();
        $('.popup .booking-results, .js-place-popup').addClass('hidden');
        $(modalOrder).removeClass('hidden');
        $(overlay).removeClass('hidden');
        $('.body').addClass('_modal-open');
    });

    $(modalClose).click(function (e) {
        e.preventDefault();
        $(modal).addClass('hidden');
        $(overlay).addClass('hidden');
        $(newsPopup).addClass('hidden');
        $('.js-news-inner').addClass('hidden');
        $('.js-place-inner').addClass('hidden');
        $('.body').removeClass('_modal-open');
    });

    $(overlay).click(function (e) {
        e.preventDefault();
        $(modal).addClass('hidden');
        $(newsPopup).addClass('hidden');
        $(overlay).addClass('hidden');
        $('.js-news-inner').addClass('hidden');
        $('.js-place-inner').addClass('hidden');
        $('.body').removeClass('_modal-open');
    });

    //Всплывашка на новостях
    let newsLink = $('.js-news-link'),
        newsPopup = $('.js-news-popup');
    $(newsLink).click(function (e) {
        e.preventDefault();
        let newsItem = $(this).attr('data-link');
        $(newsPopup).filter('[data-link='+newsItem+']').removeClass('hidden');
        $(overlay).removeClass('hidden');
        $('.js-news-inner').removeClass('hidden');
        $('.body').addClass('_modal-open');
    });

    $('.js-news-inner .overlay').click(function (e) {
        e.preventDefault();
        $(modal).addClass('hidden');
        $(newsPopup).addClass('hidden');
        $(overlay).addClass('hidden');
        $('.js-news-inner').addClass('hidden');
        $('.body').removeClass('_modal-open');
    });

    //Открываем оформление заказа
    $('.js-popup-place').click(function (e) {
        e.preventDefault();
        $(overlay).removeClass('hidden');
        $('.js-place-inner').removeClass('hidden');
        $('.body').addClass('_modal-open');
    });

    $('.js-place-inner .overlay').click(function (e) {
        e.preventDefault();
        $(overlay).addClass('hidden');
        $('.js-place-inner').addClass('hidden');
        $('.body').removeClass('_modal-open');
    });

    $('.js-place-popup').click(function (e) {
        e.preventDefault();
    });

    if ($(window).width() <= 1020) {
        $('.js-modal-order-btn').text('Оформить');
    }

    $('.js-add-comment').click(function () {
        $('.js-comment-form').removeClass('hidden');
        $(this).addClass('hidden');
    })

    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});

document.addEventListener('DOMContentLoaded', function () {
    GreenAudioPlayer.init({
        selector: '.player',
        stopOthersOnPlay: true
    });
});