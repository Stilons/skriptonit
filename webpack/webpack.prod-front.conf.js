const path = require('path');
const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const buildWebpackConfig = merge(baseWebpackConfig, {
    mode: 'production',
    plugins: [
        new FileManagerPlugin({
            onEnd: [
                {
                    delete: [
                        "./public/assets/img/icons-svg",
                    ]
                }
            ]
        }),
        new CleanWebpackPlugin({}),
        new WebpackMd5Hash(),
        new HtmlWebpackPlugin({
            filename: './index.html',
            template: './src/twig/pages/index.twig',
        }),
    ]
});

module.exports = new Promise((resolve, reject) => {
    resolve(buildWebpackConfig)
});