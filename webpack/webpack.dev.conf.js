const path = require('path');
const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const devWebpackConfig = merge(baseWebpackConfig, {
    mode: 'development',
    devServer: {
        contentBase: path.join(__dirname, '../public'),
        compress: true,
        port: 9000,
        overlay: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: './index.html',
            template: './src/twig/pages/index.twig',
        }),
    ]
});

module.exports = new Promise((resolve, reject) => {
    resolve(devWebpackConfig)
});

