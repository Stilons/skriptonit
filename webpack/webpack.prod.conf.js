const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.conf');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const FileManagerPlugin = require('filemanager-webpack-plugin');

const buildWebpackConfig = merge(baseWebpackConfig, {
    mode: 'production',
    plugins: [
        new FileManagerPlugin({
            onEnd: [
                {
                    delete: [
                        "./public/assets/img/icons-svg",
                        "./public/**/*.map",
                    ]
                }
            ]
        }),
        new CleanWebpackPlugin({}),
    ]
});

module.exports = new Promise((resolve, reject) => {
    resolve(buildWebpackConfig)
});