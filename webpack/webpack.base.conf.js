const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const PATHS = {
    src: path.join(__dirname, '../src'),
    public: path.join(__dirname, '../public'),
    assets: 'assets/'
};

module.exports = {
    entry: {main: './src/js/app.js'},
    devtool: 'source-map',
    output: {
        path: path.resolve(__dirname, '../public'),
        filename: 'js/app.js?[hash]',
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            reloadAll: true,
                        },

                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                            config: {path: 'postcss.config.js'}
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        },
                    },
                ],
            },
            {
                test: /\.twig/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            //minimize: true
                        }
                    },
                    {
                        loader: 'twig-html-loader',
                        options: {
                            namespaces: {
                                'layouts': './src/twig/base.twig',
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(woff2|woff|ttf|eot|svg)$/,
                include: [path.resolve('./src/assets/fonts')],
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: '../[path][name].[ext]',
                },
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    name: '../[path][name].[ext]',
                },
            },
            {
                test: /\.svg$/,
                include: [path.resolve('./src/assets/img/icons-svg')],
                use: [
                    {
                        loader: 'svg-sprite-loader', options: {
                            spriteFilename: '/assets/img/sprite.svg',
                            runtimeCompat: true,
                            extract: true,
                        }
                    },
                    'svg-transform-loader',
                    'svgo-loader'
                ]
            },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "css/[name].css?[hash]",
            ignoreOrder: false,
        }),
        new SpriteLoaderPlugin({
            plainSprite: true
        }),
        new CopyWebpackPlugin([
            {from: `${PATHS.src}/${PATHS.assets}img`, to: `${PATHS.public}/${PATHS.assets}img`},
            {from: `${PATHS.src}/${PATHS.assets}fonts`, to: `${PATHS.public}/${PATHS.assets}fonts`},
            {from: `${PATHS.src}/static`, to: `${PATHS.public}`},
        ]),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
    ]
};